#!/bin/sh

GHC=ghc
MAIN_MODULE=Praeludium
SRC_DIR="./src"
TARGET_DIR="./target"
LIBS=""
OPTIONS="-Wall -Werror -O3 -threaded -c"

# color codes
RED='\x1b[1;31m'
GREEN='\x1b[1;32m'
WHITE='\x1b[1;37m'
RESET='\x1b[0m'
BAD="$RED[-]$WHITE"
GOOD="$GREEN[+]$WHITE"

echo -e '\x1b[1;33m[*] checking dependencies...\x1b[0m'

function build_env() {
    ENV_FILE="$(ls -a | grep -E '^.ghc.environment.*')"
    BUILD_ENV=0
    if [[ -z $ENV_FILE ]]; then
        echo -e "$BAD no environment file found, generating..."
        cat deps | grep '^[^-]*-[^-]*' --only-matching | xargs cabal install --lib --package-env .
    else
        cat ./deps | while read LINE; do
            # check if ./deps contains $LINE
            if [[ $(cat $ENV_FILE | grep $LINE) ]]; then
                echo -e "$GOOD dependency $GREEN$LINE$WHITE is up to date"
            else
                echo -e "$BAD dependency $RED$LINE$WHITE is out of date, rebuilding..."
                echo $LINE | grep '^[^-]*-[^-]*' --only-matching | xargs cabal install --lib --package-env .
            fi
        done
    fi
}

build_env

echo -e "\x1b[1;32m[+] changing cwd to $(dirname $0)\x1b[0m"
cd $(dirname $0)
HOME_DIR=$(pwd)
mkdir --parents $TARGET_DIR
pushd $SRC_DIR > /dev/null

if echo $LIBS | grep ','; then
    LIBS_STR=$(eval "echo -l{$LIBS}")
else
    LIBS_STR=$(eval "echo -l$LIBS")
fi

echo -e "$GOOD building $MAIN_MODULE..."
$GHC --make $MAIN_MODULE $LIBS_STR $OPTIONS || exit 1
echo -e "$GOOD cleaning up..."
rm -f *.hi *.o
popd > /dev/null
