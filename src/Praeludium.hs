{-# LANGUAGE NoImplicitPrelude, FunctionalDependencies, UndecidableInstances #-}
module Praeludium (
  module Export,
  map,
  Or((||)),
  Add((+)),
  Sub((-)),
  Mul((*)),
--  Div((/)),
  Num(negate, abs, signum, fromInteger)
  ) where

import GHC.Base as Export(Int, Char, Bool, String,
                          IO, (.), ($), (++), Functor(fmap),
                          (>>=), mapM, id, const)
import Control.Monad as Export(mapM_)
import Data.Tuple as Export(uncurry)
import Data.Functor as Export((<$>))
import qualified Data.Bool as G_Bool
import qualified GHC.Num as G_Num
import GHC.Integer as Export (Integer)

map :: Functor f => (a -> b) -> f a -> f b
map = fmap

class Or a b c | a b -> c where
  (||) :: a -> b -> c

instance Or Bool Bool Bool where
  (||) = (G_Bool.||)

class Add a b c | a b -> c where
  (+) :: a -> b -> c

instance G_Num.Num a => Add a a a where
  (+) = (G_Num.+)

class Sub a b c | a b -> c where
  (-) :: a -> b -> c

instance G_Num.Num a => Sub a a a where
  (-) = (G_Num.-)

class Mul a b c | a b -> c where
  (*) :: a -> b -> c

instance G_Num.Num a => Mul a a a where
  (*) = (G_Num.*)

-- class Div a b c | a b -> c where
--   (/) :: a -> b -> c

-- instance G_Num.Num a => Div a a a where
--   (/) = (G_Num./)

class Num a where
  negate :: a -> a
  abs :: a -> a
  signum :: a -> a
  fromInteger :: Integer -> a

instance G_Num.Num a => Num a where
  negate = G_Num.negate
  abs = G_Num.abs
  signum = G_Num.signum
  fromInteger = G_Num.fromInteger
